module com.mpp.g7.librarymanagmentsystem {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.mpp.g7.librarymanagmentsystem to javafx.fxml;
    exports com.mpp.g7.librarymanagmentsystem;
    exports com.mpp.g7.librarymanagmentsystem.controller;
    opens com.mpp.g7.librarymanagmentsystem.controller to javafx.fxml;
}